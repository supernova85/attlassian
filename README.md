**Atlassian Assignment**

This automation framework demonstrates how to write tests for Confluence.
This framework is written using Selenium WebDriver, Google guice, PageObjectModel with PageFactory and its a gradle project

---

## To run

gradlew clean build runConfluenceCloudTests (or) gradlew clean build test (or) directly run this ConfluenceCloudRunTests.xml

## Future Scope:

1. Write utility methods to take screenshots on failure
2. Add more page objects
3. Handle more assertions and conditions.
4. Implement logger using lombok Slf4j. For some reason, in my Intellij editor, lombok plugin had some problem to initalize lombok Slf4j.

---

## About Framework

1. Two config files namely env-config, test-config which are YAML files are maintained to take input from the user related to Environment related and Test related config.
2. Both the config files are provided in guice module
3. Any Page object required in a class can be injected along with the configs
4. Page Objects with locators and operations are written in the Page classes.

