package com.attlassian.confluencetesting.pagefactory;

import com.attlassian.confluencetesting.loadconfigs.testconfig.TestConfig;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;


public class SpaceDetailPage extends BasePage {

    private WikiPage wikiPage;

    private String specficPageName;
    private TestConfig testConfig;

    @Inject
    public SpaceDetailPage(WikiPage wikiPage,
                           TestConfig testConfig,
                           WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.wikiPage = wikiPage;
        this.testConfig = testConfig;
        specficPageName = testConfig.getPageRestrictionProperties().getPageName();
    }

    @FindBy(how = How.ID, using = "title-text")
    public WebElement txtSpaceTitle;

    @FindBy(how = How.ID, using = "createGlobalItem")
    public WebElement btnCreate;

    @FindBy(how = How.ID, using = "title-text")
    public WebElement txtTitle;

    @FindBy(how = How.XPATH, using = "//div[contains(text(),'Pages')]")
    public WebElement txtPages;

    @FindBy(how = How.XPATH, using = "//div[@class='item' and @data-test-id='page-tree-item']//a/div[2]/div")
    public List<WebElement> listPages;


    public WikiPage clickOnSpecificPage(String pageName) {
        for(WebElement page : listPages) {
            if (page.getText().equals(pageName)) {
                page.click();
                break;
            }
        }
        return wikiPage;
    }
}
