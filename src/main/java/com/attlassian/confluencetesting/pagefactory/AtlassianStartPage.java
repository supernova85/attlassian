package com.attlassian.confluencetesting.pagefactory;


import com.attlassian.confluencetesting.module.ConfluenceModule;
import com.attlassian.confluencetesting.util.WebDriverUtility;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Guice;


@Guice(modules = {ConfluenceModule.class})
public class AtlassianStartPage extends BasePage {

    private ConfluenceHomePage confluenceHomePage;
    private WebDriver driver;
    private WebDriverUtility webDriverUtility;

    @Inject
    public AtlassianStartPage(WebDriver driver,
                              ConfluenceHomePage confluenceHomePage,
                              WebDriverUtility webDriverUtility) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.confluenceHomePage = confluenceHomePage;
        this.webDriverUtility = webDriverUtility;
    }

    @FindBy(how = How.XPATH, using = "//a[@href=\"https://hanumanthabhilash.atlassian.net/wiki\"]/div[1]")
    public WebElement lnkConfluence;

    @FindBy(how = How.XPATH, using = "//span[@aria-label=\"Switch to...\"]")
    public WebElement lnkSwitchTo;

    @FindBy(how = How.XPATH, using = "//h1[text()='Switch to']")
    public WebElement txtSwitchTo;

    @FindBy(how = How.XPATH, using = "//span[text()='Confluence']")
    public WebElement lnkSwitchToConfluence;

    public ConfluenceHomePage clickOnConfluenceLink() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        webDriverUtility.explicitWait(lnkSwitchTo);
        lnkSwitchTo.click();
        webDriverUtility.explicitWait(txtSwitchTo);
        lnkSwitchToConfluence.click();
        return confluenceHomePage;
    }

    //We can write methods to navigate to other pages
}
