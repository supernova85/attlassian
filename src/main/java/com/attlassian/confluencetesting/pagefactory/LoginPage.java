package com.attlassian.confluencetesting.pagefactory;


import com.attlassian.confluencetesting.loadconfigs.envconfig.EnvConfig;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    private AtlassianStartPage atlassianStartPage;
    private EnvConfig envConfig;
    private WebDriver driver;

    @Inject
    public LoginPage(EnvConfig envConfig,
                     WebDriver driver,
                     AtlassianStartPage atlassianStartPage) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.envConfig = envConfig;
        this.atlassianStartPage = atlassianStartPage;

    }

    @FindBy(how = How.XPATH, using = "//div/h5[text()='Log in to your account']")
    public WebElement txtLoginToYourAccount;

    @FindBy(how = How.ID, using = "username")
    public WebElement inputEmail;

    @FindBy(how = How.ID, using = "password")
    public WebElement inputPassword;

    @FindBy(how = How.XPATH, using = "//span[text()='Continue']")
    public WebElement btnContinue;

    @FindBy(how = How.XPATH, using = "//span[text()='Log in with Google']")
    public WebElement btnLoginWithGoogle;

    @FindBy(how = How.XPATH, using = "//span[text()='Log in with Microsoft']")
    public WebElement btnLoginWithMicrosoft;

    @FindBy(how = How.ID, using = "resetPassword")
    public WebElement linkCantLogin;

    @FindBy(how = How.XPATH, using = "//span[text()='Log in']")
    public WebElement btnLogin;

    @FindBy(how = How.ID, using = "signup")
    public WebElement linkSignUpForAnAccount;

    @FindBy(how = How.XPATH, using = "//span[text()='Privacy policy']")
    public WebElement linkPrivacyPolicy;

    @FindBy(how = How.XPATH, using = "//span[text()='Terms of use']")
    public WebElement linkTermsofUse;

    @FindBy(how = How.ID, using = "//a[@id='atlassian-account-link']")
    public WebElement linkLearnMoreAboutAtlassian;


    public LoginPage launchAtlassianLoginPage() {
        driver.get(envConfig.getLoginPageDetails().getUrl());
        return this;
    }

    public LoginPage enterEmailAddress(String emailAddress) {
        inputEmail.sendKeys(emailAddress);
        return this;
    }

    public LoginPage clickOnContinue() {
        btnContinue.click();
        return this;
    }

    public LoginPage enterPassword(String password) {
        if (envConfig.getLoginPageDetails().isPasswordEncrypted()) {
            password = decryptPassword(password);
        }
        inputPassword.sendKeys(password);
        return this;
    }

    public AtlassianStartPage clickOnLoginBtn() {
        btnLogin.click();
        return atlassianStartPage;
    }

    // Write methods to support google/microsoft login, privacy terms, can't login, Terms of usage etc
}
