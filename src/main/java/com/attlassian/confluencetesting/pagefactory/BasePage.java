package com.attlassian.confluencetesting.pagefactory;

import java.util.Base64;

public class BasePage {

    public String decryptPassword(String password) {
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(password));
    }
}
