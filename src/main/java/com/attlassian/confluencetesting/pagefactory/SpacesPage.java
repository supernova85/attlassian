package com.attlassian.confluencetesting.pagefactory;

import com.attlassian.confluencetesting.util.WebDriverUtility;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class SpacesPage extends BasePage {

    private SpaceDetailPage spaceDetailPage;
    private WebDriverUtility webDriverUtility;

    @Inject
    public SpacesPage(SpaceDetailPage spaceDetailPage,
                      WebDriver driver,
                      WebDriverUtility webDriverUtility) {
        PageFactory.initElements(driver, this);
        this.spaceDetailPage = spaceDetailPage;
        this.webDriverUtility = webDriverUtility;
    }

    @FindBy(how = How.XPATH, using = "//h1[@id='title-text' and contains(text(),'Space Directory')]")
    public WebElement txtSpacesDirectory;

    @FindBy(how = How.XPATH, using = "//td[contains(@class,'entity-attribute space-name')]//a")
    public List<WebElement> spacesList;

    @FindBy(how = How.XPATH, using = "//a[text()='All Spaces']")
    public WebElement lnkAllSpaces;

    @FindBy(how = How.XPATH, using = "//h2[text()='All Spaces']")
    public WebElement txtAllSpaces;

    @FindBy(how = How.CSS, using = "h1[id=title-text]")
    public WebElement txtSpecificSpaceHeader;

    public SpaceDetailPage clickOnSpeficSpace(String spaceName) {
        webDriverUtility.explicitWait(txtSpacesDirectory);
        lnkAllSpaces.click();
        webDriverUtility.explicitWait(txtAllSpaces);
        boolean spaceFlag = false;
        //Iterate over spaces list and click on specific space
        for (WebElement space : spacesList) {
            if (space.getAttribute("title").equals(spaceName)) {
                System.out.println("Space Title : " + spaceName);
                spaceFlag = true;
                space.click();
                webDriverUtility.explicitWait(txtSpecificSpaceHeader);
            }
        }

        if (!spaceFlag) {
            Assert.fail("Space is not available.");
            //log.info("There is no spaced named : " + spaceName);
            //createSpace(spaceName);
        }
        return spaceDetailPage;
    }

    // Create new space
    public void createSpace(String spaceName) {

    }

}
