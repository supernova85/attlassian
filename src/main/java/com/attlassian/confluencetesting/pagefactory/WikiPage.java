package com.attlassian.confluencetesting.pagefactory;

import com.attlassian.confluencetesting.loadconfigs.testconfig.TestConfig;
import com.attlassian.confluencetesting.util.WebDriverUtility;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class WikiPage extends BasePage {

    private TestConfig testConfig;

    private String restrictedUserVerify;

    private WebDriverUtility webDriverUtility;

    @Inject
    public WikiPage(TestConfig testConfig,
                    WebDriver driver,
                    WebDriverUtility webDriverUtility) {
        PageFactory.initElements(driver, this);
        this.webDriverUtility = webDriverUtility;
        this.testConfig = testConfig;
        restrictedUserVerify = testConfig.getPageRestrictionProperties().getRestrictions().getEditingRestricted().getUsername();
    }

    @FindBy(how = How.ID, using = "tools-menu-trigger")
    public WebElement btnToolsMenu;

    @FindBy(how = How.ID, using = "bm-restrictions-link")
    public WebElement lnkRestrictions;

    @FindBy(how = How.XPATH, using = "//span[text()='Restrictions']")
    public WebElement txtRestrictions;

    @FindBy(how = How.XPATH, using = "//div[@data-test-id='restrictions-dialog.content-mode-select']")
    public WebElement selectRestrictionType;

    @FindBy(how = How.XPATH, using = "//span[text()='Editing restricted']")
    public WebElement lnkEditingRestricted;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'placeholder')]")
    public WebElement inputUserNameRestrictions;

    @FindBy(how = How.XPATH, using = "//span[text()='Add']")
    public WebElement btnAddRestrictedUser;

    @FindBy(how = How.XPATH, using = "//span[text()='Apply']")
    public WebElement btnApplyRestrictions;

    @FindBy(how = How.XPATH, using = "//div[contains(text(),'${restrictedUserVerify}')]")
    public WebElement restrictedUser;

    public WikiPage clickOnToolsMenu() {
        webDriverUtility.explicitWait(btnToolsMenu);
        webDriverUtility.explicitWaitTillClickable(btnToolsMenu).click();
        return this;
    }

    public WikiPage clickOnRestrictionsLink() {
        lnkRestrictions.click();
        return this;
    }

    public WikiPage clickOnEditingRestricted() {
        selectRestrictionType.click();
        lnkEditingRestricted.click();
        return this;
    }

    public WikiPage enterUsersinRestrictions(String restrictedUser) {
        webDriverUtility.explicitWaitTillClickable(inputUserNameRestrictions);
        txtRestrictions.click();
        inputUserNameRestrictions.click();
        inputUserNameRestrictions.click();
        btnAddRestrictedUser.click();
        btnApplyRestrictions.click();
        return this;
    }

}
