package com.attlassian.confluencetesting.pagefactory;


import com.attlassian.confluencetesting.util.WebDriverUtility;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ConfluenceHomePage extends BasePage {

    private SpacesPage spacesPage;
    private WikiDetailPage wikiDetailPage;
    private WebDriverUtility webDriverUtility;
    private WebDriver driver;

    @Inject
    public ConfluenceHomePage(SpacesPage spacesPage,
                              WikiDetailPage wikiDetailPage,
                              WebDriver driver,
                              WebDriverUtility webDriverUtility) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.spacesPage = spacesPage;
        this.wikiDetailPage = wikiDetailPage;
        this.webDriverUtility = webDriverUtility;
    }

    @FindBy(how = How.ID, using = "productLogoGlobalItem")
    public WebElement lnkConfluenceHome;

    @FindBy(how = How.XPATH, using = "//div[text()='Spaces']")
    public WebElement lnkSpaces;

    @FindBy(how = How.ID, using = "createGlobalItem")
    public WebElement btnCreateGlobalItem;

    @FindBy(how = How.CSS, using = "div.select2-container")
    public WebElement dropDownSelectSPace;

    @FindBy(how = How.CSS, using = "div.select2-result-label")
    public List<WebElement> dropDownSPaceList;

    @FindBy(how = How.XPATH, using = "//div[contains(text(),\"Blank page\")]")
    public WebElement lnkCreateBlankPage;

    @FindBy(how = How.XPATH, using = "//button[text()='Create']")
    public WebElement btnCreate;

    public SpacesPage clickOnSpaces() {
        lnkSpaces.click();
        return spacesPage;
    }

    public ConfluenceHomePage clickOnCreateGlobalItemBtn() {
        btnCreateGlobalItem.click();
        return this;
    }

    public ConfluenceHomePage selectSpace(String spaceName) {
        dropDownSelectSPace.click();
        for (WebElement space : dropDownSPaceList) {
            if (space.getAttribute("title").equals(spaceName)) {
                space.click();
            }
        }
        return this;
    }

    public WikiDetailPage createBlankPage() {
        webDriverUtility.explicitWait(lnkCreateBlankPage);
        lnkCreateBlankPage.click();
        btnCreate.click();
        webDriverUtility.explicitWait(wikiDetailPage.txtAreaNewPageDefault);
        return wikiDetailPage;
    }

    public ConfluenceHomePage clickOnConfluenceHomelink() {
        webDriverUtility.explicitWait(lnkConfluenceHome);
        lnkConfluenceHome.click();
        return this;
    }

}
