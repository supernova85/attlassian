package com.attlassian.confluencetesting.pagefactory;


import com.attlassian.confluencetesting.util.WebDriverUtility;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class WikiDetailPage extends BasePage {

    private SpaceDetailPage spaceDetailPage;
    private WebDriverUtility webDriverUtility;
    private WebDriver driver;

    @Inject
    public WikiDetailPage(SpaceDetailPage spaceDetailPage,
                          WebDriver driver,
                          WebDriverUtility webDriverUtility) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.spaceDetailPage = spaceDetailPage;
        this.webDriverUtility = webDriverUtility;
    }


    @FindBy(how = How.CSS, using = "textarea[data-test-id=\"editor-title\"]")
    public WebElement txtPageTitle;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Publish')]")
    public WebElement btnPublish;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Did you know you can add all')]")
    public WebElement txtAreaNewPageDefault;

    public WikiDetailPage enterPageTitle(String pageName) {
        webDriverUtility.explicitWait(txtPageTitle);
        webDriverUtility.explicitWaitTillClickable(txtPageTitle);
        txtPageTitle.click();
        txtPageTitle.sendKeys(pageName);
        return this;
    }

    public SpaceDetailPage clickOnPublish() {
        btnPublish.click();
        webDriverUtility.explicitWait(spaceDetailPage.txtTitle);
        return spaceDetailPage;
    }
}
