package com.attlassian.confluencetesting.loadconfigs.envconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.inject.Singleton;
import lombok.Data;

@Data
@Singleton
public class EnvConfig {

    @JsonProperty
    public BrowserDetails browserDetails;

    @JsonProperty
    public LoginPageDetails loginPageDetails;

}
