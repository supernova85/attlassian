package com.attlassian.confluencetesting.loadconfigs.envconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class LoginPageDetails {

    @JsonProperty
    public String url = "https://id.atlassian.com/login";

    @JsonProperty
    public String username = "hanumanth.abhilash@gmail.com";

    @JsonProperty
    public String password = "Q29uZmx1ZW5jZUAxMjM=";

    @JsonProperty
    public boolean passwordEncrypted = true;

}
