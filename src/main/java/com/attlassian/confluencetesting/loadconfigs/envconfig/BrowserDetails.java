package com.attlassian.confluencetesting.loadconfigs.envconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BrowserDetails {

    @JsonProperty
    public String name = "chrome"; //write an enum to declare browser constants.

    @JsonProperty
    public int implicitWaitInSeconds = 20;
}
