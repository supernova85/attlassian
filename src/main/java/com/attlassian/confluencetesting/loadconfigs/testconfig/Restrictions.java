package com.attlassian.confluencetesting.loadconfigs.testconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Restrictions {

    @JsonProperty
    public EditingRestricted editingRestricted;
}
