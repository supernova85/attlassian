package com.attlassian.confluencetesting.loadconfigs.testconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreatePageProperties {

    @JsonProperty
    public String pageName = "TestPage1";

    @JsonProperty
    public boolean createSpaceifNotExists = true;

    @JsonProperty
    public String spaceName = "TestSpace1";

}
