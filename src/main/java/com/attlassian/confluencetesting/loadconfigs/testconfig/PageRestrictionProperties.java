package com.attlassian.confluencetesting.loadconfigs.testconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PageRestrictionProperties {

    @JsonProperty
    public String spaceName = "TestSpace1";

    @JsonProperty
    public String pageName = "TestPage1";

    @JsonProperty
    public Restrictions restrictions;

}
