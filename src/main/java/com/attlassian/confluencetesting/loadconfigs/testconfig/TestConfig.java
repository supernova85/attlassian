package com.attlassian.confluencetesting.loadconfigs.testconfig;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TestConfig {

    @JsonProperty
    public CreatePageProperties createPageProperties;

    @JsonProperty
    public PageRestrictionProperties pageRestrictionProperties;
}
