package com.attlassian.confluencetesting.loadconfigs.testconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EditingRestricted {

    @JsonProperty
    public String username = "hanumanth abhilash";

}
