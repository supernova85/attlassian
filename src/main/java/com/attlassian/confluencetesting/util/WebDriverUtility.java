package com.attlassian.confluencetesting.util;

import com.attlassian.confluencetesting.loadconfigs.envconfig.EnvConfig;
import com.google.common.base.Function;
import com.google.inject.Inject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class WebDriverUtility {

    private WebDriver driver;
    public WebDriverWait eWait;
    public Actions action;
    public JavascriptExecutor executer;
    public Wait<WebDriver> fWait;
    private EnvConfig envConfig;


    @Inject
    public WebDriverUtility(WebDriver driver,
                            EnvConfig envConfig) {
        this.envConfig = envConfig;
        this.driver = driver;
        eWait = new WebDriverWait(driver, 20);
        action = new Actions(driver);
        executer = (JavascriptExecutor) driver;
    }

    // Explicit wait for an element with Expected Conditions
    public void explicitWait(WebElement webElement) {
        eWait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public WebElement explicitWaitTillClickable(WebElement webElement) {
        return eWait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    // Fluent wait for an Web element with Expected Conditions. timeOut and pollTime are in SECONDS unit.
    public WebElement fluentWait(WebElement webElement, int timeOut, int pollTime) {
        fWait = new FluentWait<>(driver)
                .withTimeout(timeOut, TimeUnit.SECONDS)
                .pollingEvery(pollTime, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        fWait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {

                //log.info("WebElement found : " + webElement.getText());
                return webElement;
            }
        });


        fWait.until(ExpectedConditions.visibilityOf(webElement));
        return webElement;
    }

    // Fluent wait for an xpath with Expected Conditions. timeOut and pollTime are in SECONDS unit.
    public WebElement fluentWait(String xpath, int timeOut, int pollTime) {
        WebElement webElement = driver.findElement(By.xpath(xpath));

        fWait = new FluentWait<>(driver)
                .withTimeout(timeOut, TimeUnit.SECONDS)
                .pollingEvery(pollTime, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        fWait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {

                //log.info("WebElement found : " + webElement.getText());
                return webElement;
            }
        });


        fWait.until(ExpectedConditions.visibilityOf(webElement));
        return webElement;
    }
}
