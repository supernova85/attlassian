package com.attlassian.confluencetesting.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;

public class JacksonHelper {

    private ObjectMapper mapper;

    @Inject
    public JacksonHelper(ObjectMapper mapper){
        this.mapper = mapper;
    }

    public <T> T readYamlFile(String path, Class <T> c) throws IOException {
        File file = new File(path);
        return mapper.readValue(file, c);
    }

}
