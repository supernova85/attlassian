package com.attlassian.confluencetesting.util;

public enum BROWSER {

    CHROME("chrome"),
    FIREFOX("firefox");

    private String browserName;

    BROWSER(String browserName) {
        this.browserName = browserName;
    }

    public String getBrowserName() {
        return browserName;
    }
}
