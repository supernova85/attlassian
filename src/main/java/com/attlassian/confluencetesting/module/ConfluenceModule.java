package com.attlassian.confluencetesting.module;

import com.attlassian.confluencetesting.loadconfigs.envconfig.EnvConfig;
import com.attlassian.confluencetesting.loadconfigs.testconfig.TestConfig;
import com.attlassian.confluencetesting.util.BROWSER;
import com.attlassian.confluencetesting.util.JacksonHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;

public class ConfluenceModule implements Module {

    @Override
    public void configure(Binder binder) {

    }

    @Provides
    public ObjectMapper getYamlObjectMapper() {
        return new ObjectMapper(new YAMLFactory());
    }

    @Provides
    public EnvConfig getEnvConfig(JacksonHelper jacksonHelper) throws IOException {
        return jacksonHelper.readYamlFile("src/main/resources/config/env-config.yml", EnvConfig.class);
    }

    @Provides
    public TestConfig getTestConfig(JacksonHelper jacksonHelper) throws IOException {
        return jacksonHelper.readYamlFile("src/test/resources/config/test-config.yml", TestConfig.class);
    }

    @Provides
    @Singleton
    public WebDriver getDriver(WebDriver driver, EnvConfig envConfig) {
        String browserName = envConfig.getBrowserDetails().getName();

        if (browserName.equals(BROWSER.CHROME.getBrowserName())) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/chromedriver");
            driver = new ChromeDriver();
        } else if (browserName.equals(BROWSER.FIREFOX.getBrowserName())) {
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/test/resources/geckodriver");
            driver = new FirefoxDriver();
        } else {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/chromedriver");
            driver = new ChromeDriver();
        }
        System.out.println("Driver : " + driver.getClass());
        return driver;
    }

}
