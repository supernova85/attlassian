package com.attlassian.confluence.tests;

import com.attlassian.confluencetesting.loadconfigs.envconfig.EnvConfig;
import com.attlassian.confluencetesting.loadconfigs.testconfig.TestConfig;
import com.attlassian.confluencetesting.module.ConfluenceModule;
import com.attlassian.confluencetesting.pagefactory.*;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

@Guice(modules = {ConfluenceModule.class})
public class TestConfluenceOperations {

    private WebDriver driver;

    private EnvConfig envConfig;
    private TestConfig testConfig;

    private LoginPage loginPage;
    private ConfluenceHomePage confluenceHomePage;

   @Inject
   public TestConfluenceOperations(WebDriver driver,
                                   EnvConfig envConfig,
                                   TestConfig testConfig,
                                   LoginPage loginPage,
                                   ConfluenceHomePage confluenceHomePage) {
       this.driver = driver;
       this.envConfig =  envConfig;
       this.testConfig = testConfig;
       this.loginPage = loginPage;
       this.confluenceHomePage = confluenceHomePage;

    }


    @BeforeTest
    public void loginToConfluence() throws InterruptedException {

        System.out.println("Before Test : " + driver);
        driver.manage().timeouts().implicitlyWait(envConfig.getBrowserDetails().getImplicitWaitInSeconds(), TimeUnit.SECONDS);
        driver.manage().window().maximize() ;

        loginPage.launchAtlassianLoginPage()
                .enterEmailAddress(envConfig.getLoginPageDetails().getUsername())
                .clickOnContinue()
                .enterPassword(envConfig.getLoginPageDetails().getPassword())
                .clickOnLoginBtn()
                .clickOnConfluenceLink();
    }

    @Test
    public void createWikiPage() {
        WikiDetailPage wikiDetailPage = confluenceHomePage.clickOnCreateGlobalItemBtn()
                .selectSpace(testConfig.getCreatePageProperties().getSpaceName())
                .createBlankPage();

        Assert.assertEquals(driver.getTitle(),
                "Add Page - " + testConfig.getCreatePageProperties().getSpaceName() + " - Confluence");

        SpaceDetailPage spaceDetailPage = wikiDetailPage.enterPageTitle(testConfig.getCreatePageProperties().getPageName())
                .clickOnPublish();

        Assert.assertEquals(spaceDetailPage.txtTitle.getText(), testConfig.getCreatePageProperties().getPageName());
    }

    @Test
    public void editRestrictionsforWikiPage() {
        String spaceName = testConfig.getPageRestrictionProperties().getSpaceName();
        String pageName = testConfig.getPageRestrictionProperties().getPageName();

        WikiPage wikiPage = confluenceHomePage.clickOnConfluenceHomelink()
                .clickOnSpaces()
                .clickOnSpeficSpace(spaceName)
                .clickOnSpecificPage(pageName)
                .clickOnToolsMenu()
                .clickOnRestrictionsLink()
                .clickOnEditingRestricted()
                .enterUsersinRestrictions(testConfig.getPageRestrictionProperties().getRestrictions().getEditingRestricted().getUsername());
    }

    @AfterSuite
    public void finish() {
        driver.quit();
    }

}
